<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Contact;
use App\Models\Content;
use App\Models\Service;
use App\Models\Tag;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    //
    public function index(){
        $data['title'] = 'Media & Articles';

        $data['aboutsmall'] = Content::findOrFail(6);
        $data['services'] = Service::all();
        $data['page'] = 'blogs';

        $blogs = Blog::all();
        foreach ($blogs as $key => $blog) {
            $blogs[$key]['tags'] = Tag::where('blog_id',$blog['blog_id']);
            $blogs[$key]['short'] =$this->truncate(strip_tags($blog['details']));
        }

        $data['blogs'] = $blogs;

        #contacts
        $data['address'] = Contact::findOrFail(3);
        $data['phone'] = Contact::findOrFail(2);
        $data['twitter'] = Contact::findOrFail(6);
        //$this->data['linkedin'] = $this->contacts_model->get_contact(array('contact_id'=>4));
        $data['facebook'] = Contact::findOrFail(5);
        $data['youtube'] = Contact::findOrFail(7);
        //$this->data['skype'] = $this->contacts_model->get_contact(array('contact_id'=>7));
        $data['email'] = Contact::findOrFail(1);

        return view('blogs',$data);
    }

    public function single($id){
        $blog = Blog::findOrFail($id);

        $data['title'] = $blog['title'];

        $blog['tags'] = Tag::where('blog_id',$blog['blog_id']);

        $data['blog'] = $blog;
        $data['services'] = Service::all();
        $data['page'] = 'blogs';
        $data['aboutsmall'] = Content::findOrFail(6);

        #contacts
        $data['address'] = Contact::findOrFail(3);
        $data['phone'] = Contact::findOrFail(2);
        $data['twitter'] = Contact::findOrFail(6);
        //$this->data['linkedin'] = $this->contacts_model->get_contact(array('contact_id'=>4));
        $data['facebook'] = Contact::findOrFail(5);
        $data['youtube'] = Contact::findOrFail(7);
        //$this->data['skype'] = $this->contacts_model->get_contact(array('contact_id'=>7));
        $data['email'] = Contact::findOrFail(1);

        return view('single',$data);
    }

    private function truncate($text, $chars = 100) {
        if (strlen($text) <= $chars) {
            return $text;
        }
        $text = $text." ";
        $text = substr($text,0,$chars);
        $text = substr($text,0,strrpos($text,' '));
        $text = $text."...";
        return $text;
    }
}
