<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Content;
use App\Models\Partner;
use App\Models\Service;
use App\Models\Slide;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index(){
        $data['slides'] = Slide::all();
        $data['page'] = 'home';
        $data['services'] = Service::all();
        $data['title'] = "Home Page";

        #partners
        $data['partners'] = Partner::all();

        #why clinims
        $data['why'] = Content::findOrFail(7);
        $data['reason1'] = Content::findOrFail(8);
        $data['reason2'] = Content::findOrFail(9);
        $data['reason3'] =Content::findOrFail(10);

        #core module
        $data['service1'] = Service::findOrFail(2);
        $data['service2'] =  Service::findOrFail(3);
        // $this->data['service3'] = $this->services_model->get_service(array('service_id'=>4));
        $data['service4'] = Service::findOrFail(5);



        #about clinicms
        $data['aboutsmall'] = Content::findOrFail(6);

        #contacts
        $data['address'] = Contact::findOrFail(3);
        $data['phone'] = Contact::findOrFail(2);
        $data['twitter'] = Contact::findOrFail(6);
        //$this->data['linkedin'] = $this->contacts_model->get_contact(array('contact_id'=>4));
        $data['facebook'] = Contact::findOrFail(5);
        $data['youtube'] = Contact::findOrFail(7);
        //$this->data['skype'] = $this->contacts_model->get_contact(array('contact_id'=>7));
        $data['email'] = Contact::findOrFail(1);

        return view('home',$data);
    }
}
