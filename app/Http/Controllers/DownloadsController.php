<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Content;
use App\Models\Download;
use App\Models\Service;
use Illuminate\Http\Request;

class DownloadsController extends Controller
{
    //
    public function index(){
        $data['title'] = 'Downloads';

        $data['downloads'] = Download::all();

        $data['aboutsmall'] = Content::findOrFail(6);
        $data['services'] = Service::all();
        $data['page'] = 'downloads';
         #contacts
         $data['address'] = Contact::findOrFail(3);
         $data['phone'] = Contact::findOrFail(2);
         $data['twitter'] = Contact::findOrFail(6);
         //$this->data['linkedin'] = $this->contacts_model->get_contact(array('contact_id'=>4));
         $data['facebook'] = Contact::findOrFail(5);
         $data['youtube'] = Contact::findOrFail(7);
         //$this->data['skype'] = $this->contacts_model->get_contact(array('contact_id'=>7));
         $data['email'] = Contact::findOrFail(1);

        return view('downloads',$data);
    }
}
