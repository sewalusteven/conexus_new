<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Content;
use App\Models\Cpartner;
use App\Models\Partner;
use App\Models\Sdocument;
use App\Models\Service;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    //
    public function service($id){
        $service = Service::findOrFail($id);
        #load page
        $data['title'] = ucfirst($service['service']);
        $data['page'] = 'services';
        $data['services'] = Service::all();

        $data['background'] = Content::findOrFail(2);
        $data['aboutsmall'] = Content::findOrFail(6);

        $data['servicem'] = $service;

        $data['documents'] = Sdocument::where('service_id',$id)->get();

        #contacts
        $data['address'] = Contact::findOrFail(3);
        $data['phone'] = Contact::findOrFail(2);
        $data['twitter'] = Contact::findOrFail(6);
        //$this->data['linkedin'] = $this->contacts_model->get_contact(array('contact_id'=>4));
        $data['facebook'] = Contact::findOrFail(5);
        $data['youtube'] = Contact::findOrFail(7);
        //$this->data['skype'] = $this->contacts_model->get_contact(array('contact_id'=>7));
        $data['email'] = Contact::findOrFail(1);

        return view('service',$data);

    }
}
