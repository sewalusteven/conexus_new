<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Content;
use App\Models\Cpartner;
use App\Models\Partner;
use App\Models\Service;
use Illuminate\Http\Request;

class AboutController extends Controller
{


    //
    public function index(){
        #load page
        $data['title'] = 'About Us';
        $data['page'] = 'about';
         $data['services'] = Service::all();

        $data['background'] = Content::findOrFail(2);
        $data['aboutsmall'] = Content::findOrFail(6);

        $data['mission']= Content::findOrFail(3);
        $data['vision'] = Content::findOrFail(4);
        $data['values'] = Content::findOrFail(5);

        #policies
        $data['policy1'] = Content::findOrFail(11);
        $data['policy2'] = Content::findOrFail(12);
        $data['policy3'] = Content::findOrFail(13);

        #partners
        $data['partners'] = Partner::all();
        $data['cpartners'] = Cpartner::all();


        #contacts
        $data['address'] = Contact::findOrFail(3);
        $data['phone'] = Contact::findOrFail(2);
        $data['twitter'] = Contact::findOrFail(6);
        //$this->data['linkedin'] = $this->contacts_model->get_contact(array('contact_id'=>4));
        $data['facebook'] = Contact::findOrFail(5);
        $data['youtube'] = Contact::findOrFail(7);
        //$this->data['skype'] = $this->contacts_model->get_contact(array('contact_id'=>7));
        $data['email'] = Contact::findOrFail(1);

       return view('about',$data);

    }
}
