<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cpartner extends Model
{
    use HasFactory;
    protected $primaryKey = 'cpartner_id';
    public $timestamps = false;
}
