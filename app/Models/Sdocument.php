<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sdocument extends Model
{
    use HasFactory;
    protected $primaryKey = 'sdoc_id';
    public $timestamps = false;
}
