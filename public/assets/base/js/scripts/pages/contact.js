// CONTACT MAP

var PageContact = function() {

	var _init = function() {

		var mapbg = new GMaps({
			div: '#gmapbg',
			lat: 0.326949,
			lng: 32.599994,
			scrollwheel: false,
		});


		mapbg.addMarker({
            lat: 0.326949,
            lng: 32.599994,
			title: 'Your Location',
			infoWindow: {
				content: '<h3>Conexus Ltd.</h3><p>Plot 47A, Upper Kololo Terrace</p>'
			}
		});
	}

    return {
        //main function to initiate the module
        init: function() {

            _init();

        }

    };
}();

$(document).ready(function() {
    PageContact.init();
});