<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\ServicesController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\HomeController;
use \App\Http\Controllers\DownloadsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/about', [AboutController::class, 'index']);
Route::get('/downloads', [DownloadsController::class, 'index']);
Route::get('/services/detail/{id}', [ServicesController::class, 'service']);
Route::get('/blogs', [MediaController::class, 'index']);
Route::get('/blogs/single/{id}', [MediaController::class, 'single']);
Route::get('/contact', [ContactController::class, 'index']);
