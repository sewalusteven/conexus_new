<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135075931-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-135075931-3');
    </script>

    <meta charset="utf-8" />
    <title>Conexus Oil and Gas Official Website | <?=$title?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description" />
    <meta content="Sewalu Mukasa Steven" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
    <link href="{{ asset('assets/plugins/socicon/socicon.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-social/bootstrap-social.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css')  }} " rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/simple-line-icons/simple-line-icons.min.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/animate/animate.min.css')  }} " rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css')  }} " rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN: BASE PLUGINS  -->
    <link href="{{ asset('assets/plugins/revo-slider/css/settings.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/revo-slider/css/layers.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/revo-slider/css/navigation.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/cubeportfolio/css/cubeportfolio.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/owl-carousel/assets/owl.carousel.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/slider-for-bootstrap/css/slider.css')}}" rel="stylesheet" type="text/css" />
    <!-- END: BASE PLUGINS -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{asset('assets/base/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/base/css/components.css')}}" id="style_components" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/base/css/themes/yellow3.css')}}" rel="stylesheet" id="style_theme" type="text/css" />
    <link href="{{asset('assets/base/css/custom.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" />
    <!-- Google Tag Manager -->

</head>

<body class="c-layout-header-fixed c-layout-header-6-topbar">
<!-- BEGIN: LAYOUT/HEADERS/HEADER-2 -->
<!-- BEGIN: HEADER 2 -->
<header class="c-layout-header c-layout-header-6" data-minimize-offset="80">
    <div class="c-topbar">
        <div class="container">
            <!-- <marquee behavior="scroll" direction="left">Event: Supplier Development Training &nbsp;&nbsp;
                Date & Time: 22-23-24 November 2017 - 08:30-18:00&nbsp;&nbsp;
                Location: Protea Hotel, Kampala, Uganda&nbsp;&nbsp;
                Cost: USD 1950&nbsp;&nbsp;
                Event: Understanding Contractual Terms and Conditions&nbsp;&nbsp;
                Date & Time: 27-28 November 2017 - 08:30-18:00&nbsp;&nbsp;
                Location: Protea Hotel, Kampala, Uganda&nbsp;&nbsp;
                Cost: USD 1500</marquee> -->
            <div class="c-brand">
                <a href="{{url('/')}}" class="c-logo">
                    <img src="{{asset('assets/base/img/layout/logos/logo.png')}}" alt="CONEXUS" class="c-desktop-logo">
                    <img src="{{asset('assets/base/img/layout/logos/logo-1.png')}}" alt="CONEXUS" class="c-desktop-logo-inverse">
                    <img src="{{asset('assets/base/img/layout/logos/logo.png')}}" alt="CONEXUS" class="c-mobile-logo"> </a>
                <ul class="c-icons c-theme-ul">
                    <li>
                        <strong>T :</strong> <?=$phone['contact']?>
                    </li>
                    <li>
                        <strong>A :</strong> <?=str_replace('<br>',' ',$address['contact'])?>
                    </li>
                    <li>
                        <strong>E :</strong> <?=str_replace('<br>',' ',$email['contact'])?>
                    </li>
                </ul>
                <button class="c-topbar-toggler" type="button">
                    <i class="fa fa-ellipsis-v"></i>
                </button>
                <button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
                    <span class="c-line"></span>
                    <span class="c-line"></span>
                    <span class="c-line"></span>
                </button>

            </div>
        </div>
    </div>
    <div class="c-navbar">
        <div class="container">
            <!-- BEGIN: BRAND -->
            <div class="c-navbar-wrapper clearfix">
                <nav class="c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold">
                    <ul class="nav navbar-nav c-theme-nav">
                        <li class="{{ request()->is('/')? 'c-active':'' }}">
                            <a href="{{url('/')}}" class="c-link">Home
                                <span class="c-arrow c-toggler"></span>
                            </a>
                        </li>
                        <li class="{{ request()->is('about')? 'c-active':'' }}">
                            <a href="{{url('/about')}}" class="c-link">About
                                <span class="c-arrow c-toggler"></span>
                            </a>
                        </li>
                        <li class="c-menu-type-classic {{ request()->is('services/*')? 'c-active':'' }}">
                            <a href="javascript:;" class="c-link dropdown-toggle">Services
                                <span class="c-arrow c-toggler"></span>
                            </a>
                            <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                <?php foreach ($services as $key => $service){ ?>
                                <li>
                                    <a href="{{url('/services/detail/'.$service['service_id'])}}"><?=$service['service']?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="{{ request()->is('blogs')? 'c-active':'' }}">
                            <a href="{{url('/blogs')}}" class="c-link">Media
                                <span class="c-arrow c-toggler"></span>
                            </a>
                        </li>
                        <li class="{{ request()->is('downloads')? 'c-active':'' }}">
                            <a href="{{url('/downloads')}}" class="c-link">Downloads
                                <span class="c-arrow c-toggler"></span>
                            </a>
                        </li>
                        <li class="{{ request()->is('contact')? 'c-active':'' }}">
                            <a href="{{url('/contact')}}" class="c-link">Contact Us
                                <span class="c-arrow c-toggler"></span>
                            </a>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>

@yield('content')


<a name="footer"></a>
<footer class="c-layout-footer c-layout-footer-7">
    <div class="container">
        <div class="c-prefooter">
            <div class="c-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="c-content-title-1 c-title-md">
                            <h3 class="c-title c-font-uppercase c-font-bold">About CO<span class="c-theme-font">NEXUS</span></h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <?=$aboutsmall['details']?>
                    </div>

                </div>
            </div>
            <div class="c-line"></div>
            <div class="c-head">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="c-left">
                            <div class="socicon">
                                <a href="<?=$facebook['contact']?>" class="socicon-btn socicon-btn-circle socicon-solid c-font-dark-1 c-theme-on-hover socicon-facebook tooltips" data-original-title="Facebook" data-container="body"></a>
                                <a href="<?=$twitter['contact']?>" class="socicon-btn socicon-btn-circle socicon-solid c-font-dark-1 c-theme-on-hover socicon-twitter tooltips" data-original-title="Twitter" data-container="body"></a>
                                <a href="<?=$youtube['contact']?>" class="socicon-btn socicon-btn-circle socicon-solid c-font-dark-1 c-theme-on-hover socicon-youtube tooltips" data-original-title="Youtube" data-container="body"></a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-postfooter c-bg-dark-2">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 c-col">
                    <p class="c-copyright c-font-grey"><?=date('Y')?> &copy; Conexus Oil and Gas
                        <span class="c-font-grey-3">All Rights Reserved.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END: LAYOUT/FOOTERS/FOOTER-7 -->
<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div>
<!-- END: LAYOUT/FOOTERS/GO2TOP -->
<!-- BEGIN: LAYOUT/BASE/BOTTOM -->
<!-- BEGIN: CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{asset('assets/global/plugins/excanvas.min.js')}}"></script>
<![endif]-->
<script src="{{asset('assets/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/jquery.easing.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/reveal-animate/wow.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/base/js/scripts/reveal-animate/reveal-animate.js')}}" type="text/javascript"></script>
<!-- END: CORE PLUGINS -->
<!-- BEGIN: LAYOUT PLUGINS -->
<script src="{{ asset('assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/revo-slider/js/extensions/revolution.extension.video.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/fancybox/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/smooth-scroll/jquery.smooth-scroll.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js') }}" type="text/javascript"></script>
<!-- END: LAYOUT PLUGINS -->
<!-- BEGIN: THEME SCRIPTS -->
<script src="{{asset('assets/base/js/components.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/base/js/components-shop.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/base/js/app.js')}}" type="text/javascript"></script>
<script id="dsq-count-scr" src="//conexus-1.disqus.com/count.js" async></script>
<!-- END: THEME SCRIPTS -->
<!-- BEGIN: PAGE SCRIPTS -->
<!-- BEGIN: PAGE SCRIPTS -->

<script>
    $(document).ready(function()
    {
        App.init(); // init core
        var slider = $('.c-layout-revo-slider .tp-banner');
        var cont = $('.c-layout-revo-slider .tp-banner-container');
        var api = slider.show().revolution(
            {
                sliderType: "standard",
                sliderLayout: "fullscreen",
                responsiveLevels: [2048, 1024, 778, 320],
                gridwidth: [1240, 1024, 778, 320],
                gridheight: [868, 768, 960, 720],
                delay: 15000,
                startwidth: 1170,
                startheight: App.getViewPort().height,
                navigationType: "hide",
                navigationArrows: "solo",
                touchenabled: "on",
                navigation:
                    {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        onHoverStop: "on",
                        bullets:
                            {
                                style: "round",
                                enable: true,
                                hide_onmobile: false,
                                hide_onleave: true,
                                hide_delay: 200,
                                hide_delay_mobile: 1200,
                                hide_under: 0,
                                hide_over: 9999,
                                direction: "horizontal",
                                h_align: "right",
                                v_align: "bottom",
                                space: 5,
                                h_offset: 60,
                                v_offset: 60,
                            },
                    },
                spinner: "spinner2",
                fullScreenOffsetContainer: '.c-layout-header',
                shadow: 0,
                hideTimerBar: "on",
                hideThumbsOnMobile: "on",
                hideNavDelayOnMobile: 1500,
                hideBulletsOnMobile: "on",
                hideArrowsOnMobile: "on",
                hideThumbsUnderResolution: 0
            });
    }); //ready
</script>
<!-- END: PAGE SCRIPTS -->
<!-- END: LAYOUT/BASE/BOTTOM -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js-na1.hs-scripts.com/19955121.js"></script>
</body>

</html>


