@extends('layouts.app')
@section('content')
<div class="c-layout-page">
    <!-- BEGIN: PAGE CONTENT -->
    <!-- BEGIN: LAYOUT/SLIDERS/REVO-SLIDER-9 -->
    <section class="c-layout-revo-slider c-layout-revo-slider-9" dir="ltr">
        <div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile">
            <div class="tp-banner rev_slider" data-version="5.0">
                <ul>
                    <!--BEGIN: SLIDE #2 -->
                    <?php foreach ($slides as $key => $slide){ ?>
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                            <img alt="" src="{{asset('hslides/'.$slide['tmp_name'])}}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <!--BEGIN: MAIN TITLE -->
                            <div class="tp-caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="[-300,-300, -100, -100]" data-voffset="-120" data-speed="500" data-start="1000" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;:s:500;e:Back.easeInOut;"
                                 data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
                                 data-endspeed="600">
                                <h3 class="c-main-title c-font-44 c-font-bold c-font-uppercase c-font-white "> <?=$slide['title']?>  </h3>
                                <p class="c-sub-title c-font-24 c-font-white "><?=$slide['caption']?> </p>
                                <a href="<?=$slide['url']?>" class="c-action-btn btn btn-xlg c-btn-square c-btn-bold c-btn-border-2x c-btn-white c-btn-uppercase"><?=$slide['link_text']?></a>
                            </div>
                            <!--END -->
                        </li>
                    <?php  } ?>
                    <!--END -->
                </ul>
            </div>
        </div>
    </section>

    <!-- END: LAYOUT/SLIDERS/REVO-SLIDER-9 -->
    <!-- BEGIN: CONTENT/BARS/BAR-3 -->
    <div class="c-content-box c-size-md c-bg-dark">
        <div class="container">
            <div class="c-content-bar-3">
                <div class="row">
                    <div class="col-md-7">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold">DEDICATED STAFF</h3>
                            <p class="c-font-uppercase">We have highly skilled employees nationwide, on staff 24 hours a day, seven days a week</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-2">
                        <div class="c-content-v-center" style="height: 90px;">
                            <div class="c-wrapper">
                                <div class="c-body">
                                    <a href="{{url('/contact')}}" class="btn btn-md c-btn-square c-btn-border-2x c-theme-btn c-btn-uppercase c-btn-bold">Reach Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/BARS/BAR-3 -->
    <!-- BEGIN: CONTENT/MISC/WHY-CHOOSE-US-1 -->
    <div class="c-content-box c-size-lg c-bg-grey-1">
        <div class="container">
            <div class="">
                <div class="row">
                    <div class="col-md-7">
                        <div class="c-content-feature-5">
                            <div class="c-content-title-1 wow amimate fadeInDown">
                                <h3 class="c-left c-font-dark c-font-uppercase c-font-bold">Why
                                    <br/>CONEXUS ?</h3>
                                <div class="c-line-left c-bg-blue-3 c-theme-bg"></div>
                            </div>
                            <div class="c-text wow animate fadeInLeft"> <?=$why['details']?> </div>
                            <a href="{{url('/about')}}" class="btn c-btn-uppercase btn-md c-btn-bold c-btn-square c-theme-btn wow animate fadeIn">Explore</a>
                            <img class="c-photo img-responsive wow animate fadeInUp" width="420" alt="" src="{{asset('why.jpg')}}" /> </div>
                    </div>
                    <div class="col-md-5">
                        <div class="c-content-accordion-1 c-theme wow animate fadeInRight">
                            <div class="panel-group" id="accordion" role="tablist">
                                <div class="panel">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a class="c-font-bold c-font-uppercase c-font-19" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> <?=$reason1['title']?> </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body c-font-18"><?=$reason1['details']?> </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed c-font-uppercase c-font-bold c-font-19" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> <?=$reason2['title']?> </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body c-font-18"><?=$reason2['details']?> </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed c-font-uppercase c-font-bold c-font-19" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> <?=$reason3['title']?> </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body c-font-18"><?=$reason3['details']?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/MISC/WHY-CHOOSE-US-1 -->
    <!-- BEGIN: CONTENT/TILES/TILE-3 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="c-content-tile-grid c-bs-grid-reset-space" data-auto-height="true">
            <div class="c-content-title-1 wow animate fadeInDown">
                <h3 class="c-font-uppercase c-center c-font-bold">Services We Do</h3>
                <div class="c-line-center"></div>
            </div>
            <div class="row wow animate fadeInUp">
                <div class="col-md-6">
                    <div class="c-content-tile-1 c-bg-green">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="c-tile-content c-content-v-center" data-height="height">
                                    <div class="c-wrapper">
                                        <div class="c-body c-center">
                                            <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white"> <?=$service1['service']?> </h3>
                                            <a href="<?=url('/services/detail/'.$service1['service_id'])?>" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="c-tile-content c-arrow-right c-arrow-green c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="<?=url('/services/detail/'.$service1['service_id'])?>">
                                                <i class="icon-link"></i>
                                            </a>
                                            <a href="<?=asset('cservices/'.$service1['tmp_name'])?>" data-lightbox="fancybox" data-fancybox-group="gallery-4">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?=asset('cservices/'.$service1['tmp_name'])?>)"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="c-content-tile-1 c-bg-brown-2">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="c-tile-content c-content-v-center" data-height="height">
                                    <div class="c-wrapper">
                                        <div class="c-body c-center">
                                            <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white"> <?=$service2['service']?> </h3>
                                            <a href="<?=url('/services/detail/'.$service2['service_id'])?>" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="c-tile-content c-arrow-right c-arrow-brown-2 c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="<?=url('/services/detail/'.$service2['service_id'])?>">
                                                <i class="icon-link"></i>
                                            </a>
                                            <a href="<?=asset('cservices/'.$service2['tmp_name'])?>" data-lightbox="fancybox" data-fancybox-group="gallery-4">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?=asset('cservices/'.$service2['tmp_name'])?>)"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="c-content-tile-1 c-bg-red-2">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="c-tile-content c-arrow-left c-arrow-red-2 c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="<?=url('/services/detail/'.$service4['service_id'])?>">
                                                <i class="icon-link"></i>
                                            </a>
                                            <a href="<?=asset('cservices/'.$service4['tmp_name'])?>" data-lightbox="fancybox" data-fancybox-group="gallery-4">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?=asset('cservices/'.$service4['tmp_name'])?>)"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="c-tile-content c-content-v-center" data-height="height">
                                    <div class="c-wrapper">
                                        <div class="c-body c-center">
                                            <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white"> <?=$service4['service']?> </h3>
                                            <a href="<?=url('/services/detail/'.$service4['service_id'])?>" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/TILES/TILE-3 -->


    <!-- BEGIN: CONTENT/SLIDERS/CLIENT-LOGOS-1 -->
    <div class="c-content-box c-size-md c-bg-white" style="background-image: url(<?=asset('assets/base/img/content/backgrounds/bg-48.jpg')?>)">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-client-logos-slider-1" data-slider="owl">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-white c-font-bold">Extended Experience</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>
                <!-- End-->
                <!-- Begin: Owlcarousel -->
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-items="5" data-desktop-items="4" data-desktop-small-items="3" data-tablet-items="3" data-mobile-small-items="1" data-auto-play="false" data-rtl="false" data-slide-speed="5000"
                     data-auto-play-hover-pause="true">
                    <?php foreach ($partners as $key => $partner){ ?>
                    <div class="item">
                        <a href="<?=$partner['url']?>">
                            <img src="<?=asset('cpartners/'.$partner['tmp_name'])?>" alt="<?=$partner['partner']?>" />
                        </a>
                    </div>

                    <?php } ?>


                </div>
                <!-- End-->
            </div>
            <!-- End-->
        </div>
    </div>
    <!-- END: CONTENT/SLIDERS/CLIENT-LOGOS-1 -->
    <!-- END: PAGE CONTENT -->
</div>
@endsection
