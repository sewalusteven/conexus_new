@extends('layouts.app')
@section('content')
    <div class="c-layout-page">
        <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
        <div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
            <div class="container">
                <div class="c-page-title c-pull-left">
                    <h3 class="c-font-uppercase c-font-sbold"><?=$title?></h3>
                </div>
                <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                    <li>
                        <a href="#">Pages</a>
                    </li>
                    <li>/</li>
                    <li class="c-state_active"><?=$title?></li>
                </ul>
            </div>
        </div>
        <!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
        <!-- BEGIN: PAGE CONTENT -->
        <!-- BEGIN: BLOG LISTING -->
        <div class="c-content-box c-size-md">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="c-content-blog-post-card-1-grid">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>File</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(sizeof($downloads) === 0){
                                            echo "<tr><th colspan='2'>No Files available for download yet</th></tr>";
                                        } else {
                                            $count = 1;
                                            foreach ($downloads as $download){
                                                echo "<tr>
                                                        <th>".$count."</th>
                                                        <th><a href='".url('files/'.$download['path'])."'>".$download['name']."</a></th>
                                                      </tr>";
                                                $count++;
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- BEGIN: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
                        <a class="twitter-timeline" data-height="900" data-link-color="#FAB81E" href="https://twitter.com/Conexusoilgas?ref_src=twsrc%5Etfw">Tweets by Conexusoilgas</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                        <!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END: BLOG LISTING  -->
        <!-- END: PAGE CONTENT -->
    </div>
@endsection
