@extends('layouts.app')
@section('content')
    <div class="c-layout-page">
        <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
        <div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
            <div class="container">
                <div class="c-page-title c-pull-left">
                    <h3 class="c-font-uppercase c-font-sbold"><?=$title?></h3>
                </div>
                <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                    <li>
                        <a href="#">Pages</a>
                    </li>
                    <li>/</li>
                    <li>
                        <a href="{{url('/blogs')}}">Media & Articles</a>
                    </li>
                    <li>/</li>
                    <li class="c-state_active"><?=$title?></li>
                </ul>
            </div>
        </div>
        <!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
        <!-- BEGIN: PAGE CONTENT -->
        <!-- BEGIN: BLOG LISTING -->
        <div class="c-content-box c-size-md">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="c-content-blog-post-1-view">
                            <div class="c-content-blog-post-1">

                                <?php
                                if($blog['tmp_name'] != NULL){
                                ?>
                                <div class="c-media">
                                    <img src="{{asset('cblogs/'.$blog['tmp_name'])}}"  height="460" alt='<?=$blog['title']?>'>
                                </div>
                                <?php
                                }
                                ?>

                                <div class="c-title c-font-bold c-font-uppercase">
                                    <a href="#"><?=$blog['title']?></a>
                                </div>
                                <div class="c-panel c-margin-b-30">
                                    <div class="c-author">
                                        <a href="#">By
                                            <span class="c-font-uppercase"><?=$blog['author']?></span>
                                        </a>
                                    </div>
                                    <div class="c-date">on
                                        <span class="c-font-uppercase"><?=date("d M Y, h:i A", strtotime($blog['created']))?></span>
                                    </div>
                                    <ul class="c-tags c-theme-ul-bg">
                                        <?php
                                        foreach ($blog['tags'] as $keym => $tag) {

                                            echo'<li>'.$tag['tag'].'</li>';

                                        }
                                        ?>

                                    </ul>

                                </div>
                                <div class="c-desc">
                                    <?=$blog['details']?>
                                </div>
                                <div class="c-comments">
                                    <div id="disqus_thread"></div>
                                    <script>


                                        var disqus_config = function () {
                                            this.page.url = "<?=url('/')?>";  // Replace PAGE_URL with your page's canonical URL variable
                                            this.page.identifier = <?=$blog['blog_id']?>; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                        };

                                        (function() { // DON'T EDIT BELOW THIS LINE
                                            var d = document, s = d.createElement('script');
                                            s.src = 'https://conexus-1.disqus.com/embed.js';
                                            s.setAttribute('data-timestamp', +new Date());
                                            (d.head || d.body).appendChild(s);
                                        })();
                                    </script>
                                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- BEGIN: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
                        <a class="twitter-timeline" data-height="900" data-link-color="#FAB81E" href="https://twitter.com/Conexusoilgas?ref_src=twsrc%5Etfw">Tweets by Conexusoilgas</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                        <!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END: BLOG LISTING  -->
        <!-- END: PAGE CONTENT -->
    </div>
@endsection
