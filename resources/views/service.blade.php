@extends('layouts.app')
@section('content')
    <div class="c-layout-page">

        <!-- BEGIN: PAGE CONTENT -->
        <!-- BEGIN: CONTENT/FEATURES/FEATURES-13-4 -->
        <!-- BEGIN: FEATURES 13.4 -->
        <div class="c-content-box c-size-md c-no-padding c-bg-img-center" style="background-image: url({{asset('cservices/'.$servicem['tmp_name'])}})">
            <div class="c-content-feature-13">
                <div class="row c-reset">
                    <div class="col-md-6 col-md-offset-6 c-bg-white">
                        <div class="c-feature-13-container">
                            <div class="c-content-title-1">
                                <h3 class="c-font-uppercase c-font-bold"><?=$title?>
                                </h3>
                                <div class="c-line-left c-theme-bg"></div>
                            </div>
                            <div class="row">
                                <div class="c-feature-13-tile">
                                    <i class="icon-folder c-theme-font c-font-24"></i>
                                    <div class="c-feature-13-content">
                                        <?=$servicem['description']?>
                                    </div>

                                    <?php if(sizeof($documents) != 0){ ?>
                                    <i class="fa fa-download c-theme-font c-font-24"></i>
                                    <div class="c-feature-13-content">
                                        <h3>Downloads</h3>
                                    </div>
                                    <ul class="c-content-list-1">
                                        <?php foreach($documents as $key => $document){ ?>
                                        <li><a href="{{url('uploads/'.$document['tmp_name'])}}"><?=$document['title']?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: FEATURES 13-4 -->
        <!-- END: CONTENT/FEATURES/FEATURES-13-4 -->



        <!-- END: PAGE CONTENT -->
    </div>
@endsection
