@extends('layouts.app')

@section('content')
    <div class="c-layout-page">

        <!-- BEGIN: PAGE CONTENT -->
        <!-- BEGIN: CONTENT/MISC/LATEST-ITEMS-3 -->
        <div class="c-content-box c-size-md  c-bg-grey-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="c-content-media-1 c-bordered wow animated fadeInLeft" style="min-height: 380px;">
                            <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg"><?=$mission['title']?></div>
                            <a href="#" class="c-title c-font-uppercase c-theme-on-hover c-font-bold"><?=$mission['details']?></a>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="c-content-media-1 c-bordered wow animated fadeInLeft" style="min-height: 380px;">
                            <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg"><?=$vision['title']?></div>
                            <a href="#" class="c-title c-font-uppercase c-theme-on-hover c-font-bold"><?=$vision['details']?></a>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="c-content-media-1 c-bordered wow animated fadeInLeft" style="min-height: 380px;">
                            <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg"><?=$values['title']?></div>
                            <a href="#" class="c-title c-font-uppercase c-theme-on-hover c-font-bold"><?=$values['details']?></a>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- END: CONTENT/MISC/LATEST-ITEMS-3 -->
        <!-- BEGIN: CONTENT/FEATURES/FEATURES-13-1 -->
        <!-- BEGIN: FEATURES 13.1 -->
        <div class="c-content-box c-size-md c-no-padding c-bg-img-center" style="background-image: url({{asset('assets/base/img/content/backgrounds/bg-48.jpg')}})">
            <div class="c-content-feature-13">
                <div class="row c-reset">
                    <div class="col-md-6 c-bg-dark">
                        <div class="c-feature-13-container">
                            <div class="c-content-title-1">
                                <h3 class="c-center c-font-uppercase c-font-white c-font-bold">About
                                    <span class="c-theme-font">Conexus</span></h3>
                                <div class="c-line-center c-theme-bg"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 c-feature-13-tile">
                                    <div class="c-feature-13-content">
                                        <?=$background['details']?>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: FEATURES 13-1 -->
        <!-- END: CONTENT/FEATURES/FEATURES-13-1 -->
        <!-- BEGIN: CONTENT/MISC/SERVICES-3 -->
        <div class="c-content-box c-size-md c-bg-white">
            <div class="container">
                <div class="c-content-feature-2-grid">
                    <div class="c-content-title-1">
                        <h3 class="c-font-uppercase c-center c-font-bold">Conexus Policies</h3>
                        <div class="c-line-center"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover">
                                <h3 class="c-font-uppercase c-title"><?=$policy1['title']?></h3>
                                <?=$policy1['details']?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover">
                                <h3 class="c-font-uppercase c-title"><?=$policy2['title']?></h3>
                                <?=$policy2['details']?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover">
                                <h3 class="c-font-uppercase c-title"><?=$policy3['title']?></h3>
                                <?=$policy3['details']?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- END: CONTENT/MISC/SERVICES-3 -->
        <!-- BEGIN: CONTENT/SLIDERS/CLIENT-LOGOS-1 -->
        <div class="c-content-box c-size-md c-bg-white" style="background-image: url({{asset('assets/base/img/content/backgrounds/bg-48.jpg')}})">
            <div class="container">
                <div class="c-content-client-logos-slider-1" data-slider="owl">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1">
                        <h3 class="c-center c-font-uppercase c-font-white c-font-bold">Extended Experience</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div>
                    <!-- End-->
                    <!-- Begin: Owlcarousel -->
                    <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-items="5" data-desktop-items="4" data-desktop-small-items="3" data-tablet-items="3" data-mobile-small-items="1" data-auto-play="false" data-rtl="false" data-slide-speed="5000"
                         data-auto-play-hover-pause="true">
                        <?php foreach ($partners as $key => $partner){ ?>
                        <div class="item">
                            <a href="<?=$partner['url']?>">
                                <img src="{{asset('cpartners/'.$partner['tmp_name'])}}" alt="<?=$partner['partner']?>" />
                            </a>
                        </div>

                        <?php } ?>


                    </div>
                    <!-- End-->
                </div>

                <!-- End-->
            </div>
        </div>
        <!-- END: CONTENT/SLIDERS/CLIENT-LOGOS-1 -->
        <div class="c-content-box c-size-md c-bg-white">
            <div class="container">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Our Partners</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <?php foreach ($cpartners as $key => $cpartner){ ?>
                        <tr>
                            <th class="warning"><?=$cpartner['partner']?></th>
                            <td class="active"><?=$cpartner['explanation']?></td>
                        </tr>

                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>




        <!-- END: PAGE CONTENT -->
    </div>
@endsection
